const {app, BrowserWindow} = require('electron');
const url = require('url');
const path = require('path');

let mainWindow; // declared here to secure it from garbage collection
global.sharedObj = {};

app.on('ready', () => {

    mainWindow = new BrowserWindow({
        width: 740,
        height: 380,
        useContentSize: true,
        title: "Prime Time"
    });

    // load the main page
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'html/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    mainWindow.on('closed', function () {
        mainWindow = null;
    });

    app.on('activate-with-no-open-windows', function () {
        mainWindow.show();
    });
});
