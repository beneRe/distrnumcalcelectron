const calc = require('../js/modules/calc').Calculator;
const $ = require('jquery');
const io = require('socket.io-client');
let status = {active: true, registered: false};
let email;
let host;
let password;

$('#loginform').on('submit', (e) => {

    host = $('#host').val();
    email = $('#loginemail').val();
    password = $('#loginpw').val();

    postData(`${host}/acc/login`, {nativeClient: true, email, password}).then( (json) => {
       if (json.result) {
           $('#gridwrap').css('display', 'grid');
           $('#userforms').css('display', 'none');
           initSocket();
       }
       else {
           $('#errfield').text('Error on login!');
       }
    }).catch( (err) => {
        console.log(err);
    });

    e.preventDefault();
});

$('#registerform').on('submit', (e) => {

    e.preventDefault();

    host = $('#host').val();
    email = $('#registeremail').val();
    password = $('#registerpassword').val();
    if (password !== $('#registerpasswordconfirm').val())
        return $('#errfield').text('PWs dont match!');

    postData(`${host}/acc/newAcc`, {nativeClient: true, email, password}).then( (json) => {
        if (json.result) {
            $('#gridwrap').css('display', 'grid');
            $('#userforms').css('display', 'none');
            initSocket();
        }
        else {
            $('#errfield').text('Error on register!');
        }
    }).catch( (err) => {
        console.log(err);
    });

});

function postData(url = '', data = {}) {
    console.log(url);
    // Default options are marked with *
    return fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
        .then(response => response.json())
        .catch(err => console.log(err)); // parses JSON response into native Javascript objects
}

const initSocket = function () {

    const socket = io.connect(host);

    socket.emit('register', JSON.stringify({email}));

    socket.on('numberAssignment', (data) => {

        data = JSON.parse(data);

        if (!status.registered)
            socket.emit('register', JSON.stringify({email}));

        const number = data.number;
        const persScore = data.persScore;
        const overallScore = data.overallScore;
        const contribution = Math.floor((persScore / overallScore) * 100);

        $('#currNum').text(`Current Number: ${number}`);
        $('#persScore').text(`Personal Score: ${persScore}`);
        $('#overallScore').text(`Overall Score: ${overallScore}`);
        $('#contrBar').css('width', `${contribution}%`);

        const result = calc.calculatePrimeNumber(number);

        if (status.active)
            socket.emit('numberProcessed', JSON.stringify({result, number, email}));
        else
            localStorage.setItem('numberbuffer', JSON.stringify({result, number, email}));
    });

    socket.on('lock', () => {
        status.active = false;
        console.log('Client process locked by server');
        socket.emit('locked');
    });

    socket.on('unlock', () => {
        status.active = true;
        console.log('unlocked, sending last result to server: ');
        console.log(localStorage.getItem('numberbuffer'));
        socket.emit('numberProcessed', localStorage.getItem('numberbuffer'));
    });

    socket.on('registrationSuccessful', () => {
        status.registered = true;
        console.log('registered on server');
    });
};
