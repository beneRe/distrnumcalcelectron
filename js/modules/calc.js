module.exports.Calculator = class Calculator {

    static calculatePrimeNumber(number) {

        if (number === 2)
            return true;

        if (number % 2 === 0)
            return false;

        for (let i = 2; i < number; i++) {
            if (number % i === 0)
                return false;
        }

        return true;
    }
};